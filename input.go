// This file is modified version of
// https://github.com/Jguer/yay/blob/next/pkg/text/input.go
// author: Jguer
package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
)

func GetInput(defaultValue string, noConfirm bool) (string, error) {
	if defaultValue != "" || noConfirm {
		fmt.Println(defaultValue)
		return defaultValue, nil
	}

	reader := bufio.NewReader(os.Stdin)
	buf, overflow, err := reader.ReadLine()
	if err != nil {
		return "", err
	}

	if overflow {
		return "", errors.New("input too long")
	}

	return string(buf), nil
}
