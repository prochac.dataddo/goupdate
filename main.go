package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"
)

type Module struct {
	Path    string
	Version string
	Time    time.Time
	Update  *struct {
		Path    string
		Version string
		Time    time.Time
	}
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	err := exec.CommandContext(ctx, "go", "mod", "tidy").Run()
	execErr(err)

	cmd := exec.CommandContext(ctx, "go", "list", "-m",
		// only direct external modules
		"-f", "{{if not (or .Indirect .Main .Replace)}}{{.Path}}{{end}}",
		"all")
	cmdOut, err := cmd.Output()
	execErr(err)

	var allModules []string
	scanner := bufio.NewScanner(bytes.NewReader(cmdOut))
	for scanner.Scan() {
		module := scanner.Text()
		allModules = append(allModules, module)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	type availableUpdate struct {
		Name                   string
		OldVersion, NewVersion semVer
	}

	var availableUpdates []availableUpdate
	for i, module := range allModules {
		d := float64(i) / float64(len(allModules))
		fmt.Printf("\r%3.0f%%|%-66s| %3d/%-3d",
			d*100,
			strings.Repeat("█", int(d*66)),
			i+1, len(allModules),
		)
		if i == 0 {
			fmt.Println("\r" + boldCode + "Up to date:" + resetCode)
		}

		cmd := exec.CommandContext(ctx, "go", "list", "-m", "-json", "-u", module)
		b, err := cmd.Output()
		execErr(err)
		var moduleInfo Module
		err = json.Unmarshal(b, &moduleInfo)
		execErr(err)

		currentVersion, err := parseSemVer(moduleInfo.Version)
		execErr(err)

		if moduleInfo.Update == nil {
			fmt.Printf("\r%s %s\n", module, currentVersion.String())
			continue
		}
		moduleUpdate, err := parseSemVer(moduleInfo.Update.Version)
		execErr(err)

		availableUpdates = append(availableUpdates, availableUpdate{
			Name:       module,
			OldVersion: currentVersion,
			NewVersion: moduleUpdate,
		})
	}
	if len(availableUpdates) == 0 {
		fmt.Println(boldCode + green + "\nNothing to update" + resetCode)
		return
	}

	w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', 0)
	for i, au := range availableUpdates {
		if i == 0 {
			fmt.Println(boldCode + "\nAvailable updates:" + resetCode)
		}

		diffIndex := findSemVerDiff(au.OldVersion, au.NewVersion)

		_, _ = fmt.Fprintf(w, "%d\t%s\t%s\t%s\n",
			i+1, au.Name,
			au.OldVersion.ColorTail(diffIndex, red), au.NewVersion.ColorTail(diffIndex, green),
		)
	}
	_ = w.Flush()

	fmt.Println(boldCode + "\nPackages to update: (eg: 1 2 3, 1-3, ^4 or all)" + resetCode)
	input, err := GetInput("", false)
	if err != nil {
		log.Fatal(err)
	}
	if input == "all" {
		fmt.Println(boldCode + "Updating all packages" + resetCode)
	}
	include, exclude := ParseNumberMenu(input)
	for i, au := range availableUpdates {
		if input != "all" {
			if exclude.Get(i+1) || !include.Get(i+1) {
				fmt.Println(lightGray+"ignoring:", au.Name, resetCode)
				continue
			}
		}
		latestPkgVersion := au.Name + "@" + au.NewVersion.String()
		fmt.Println("Updating:", latestPkgVersion)
		cmd := exec.CommandContext(ctx, "go", "get", latestPkgVersion)
		execErr(cmd.Run())
	}

	err = exec.CommandContext(ctx, "go", "mod", "tidy").Run()
	execErr(err)
}

func execErr(err error) {
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			_, _ = fmt.Fprintln(os.Stderr, string(ee.Stderr))
		}
		log.Fatal(err)
	}
}

type semVer struct {
	Major, Minor, Patch int
	PreRelease          string
	Timestamp           string
	RevisionIdentifier  string
	Incompatible        bool
}

func (ver semVer) String() string {
	s := fmt.Sprintf("v%d.%d.%d", ver.Major, ver.Minor, ver.Patch)
	if ver.Incompatible {
		s += "+incompatible"
	}
	if ver.PreRelease != "" {
		s += "-" + ver.PreRelease
	}
	if ver.Timestamp != "" {
		s += "-" + ver.Timestamp
	}
	if ver.RevisionIdentifier != "" {
		s += "-" + ver.RevisionIdentifier
	}
	return s
}

const (
	black     = "\x1b[30m"
	red       = "\x1b[31m"
	green     = "\x1b[32m"
	yellow    = "\x1b[33m"
	blue      = "\x1b[34m"
	magenta   = "\x1b[35m"
	cyan      = "\x1b[36m"
	lightGray = "\x1b[37m"

	boldCode = "\x1b[1m"

	resetCode = "\x1b[0m"
)

func (ver semVer) ColorTail(index int, color string) string {
	format := "v"
	for i := 0; i < 3; i++ {
		if i != 0 {
			format += "."
		}
		if i == index {
			format += color
		}
		format += "%d"
	}

	s := fmt.Sprintf(format, ver.Major, ver.Minor, ver.Patch)
	if ver.Incompatible {
		s += "+incompatible"
	}
	if ver.PreRelease != "" {
		if index == 3 {
			s += color
		}
		s += "-" + ver.PreRelease
	}
	if ver.Timestamp != "" {
		if index == 4 {
			s += color
		}
		s += "-" + ver.Timestamp
	}
	if ver.RevisionIdentifier != "" {
		s += "-" + ver.RevisionIdentifier
	}
	s += resetCode
	return s
}

func parseSemVer(s string) (semVer, error) {
	var ver semVer

	s = strings.TrimPrefix(s, "v")

	if strings.HasSuffix(s, "+incompatible") {
		ver.Incompatible = true
		s = s[:len(s)-len("+incompatible")]
	}

	sl := strings.SplitN(s, ".", 3)
	if len(sl) != 3 {
		return semVer{}, errors.New("full semantic version exprected")
	}

	var err error
	ver.Major, err = strconv.Atoi(sl[0])
	if err != nil {
		return semVer{}, fmt.Errorf("can't parse major version: %v", err)
	}
	ver.Minor, err = strconv.Atoi(sl[1])
	if err != nil {
		return semVer{}, fmt.Errorf("can't parse minor version: %v", err)
	}
	ver.Patch, err = strconv.Atoi(sl[2])
	if err != nil {
		sl := strings.Split(sl[2], "-")
		switch len(sl) {
		case 2:
			ver.Patch, err = strconv.Atoi(sl[0])
			if err != nil {
				return semVer{}, fmt.Errorf("can't parse patch version: %v", err)
			}
			ver.PreRelease = sl[1]
		case 3:
			ver.Patch, err = strconv.Atoi(sl[0])
			if err != nil {
				return semVer{}, fmt.Errorf("can't parse patch version: %v", err)
			}
			ver.Timestamp = sl[1]
			ver.RevisionIdentifier = sl[2]
		default:
			return semVer{}, fmt.Errorf("can't parse patch version: %v", err)
		}
	}
	return ver, nil
}

func findSemVerDiff(s1, s2 semVer) int {
	if s1.Major != s2.Major {
		return 0
	}
	if s1.Minor != s2.Minor {
		return 1
	}
	if s1.Patch != s2.Patch {
		return 2
	}
	if s1.PreRelease != s2.PreRelease {
		return 3
	}
	if s1.Timestamp != s2.Timestamp || s1.RevisionIdentifier != s2.RevisionIdentifier {
		return 4
	}
	return -1
}
