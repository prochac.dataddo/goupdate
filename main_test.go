package main

import (
	"reflect"
	"testing"
)

func Test_parseSemVer(t *testing.T) {
	tests := []struct {
		s       string
		want    semVer
		wantErr bool
	}{
		{
			s: "63.4.0+incompatible",
			want: semVer{
				Major:        63,
				Minor:        4,
				Patch:        0,
				Incompatible: true,
			},
		},
		{
			s: "1.2.0-rc.10",
			want: semVer{
				Major:      1,
				Minor:      2,
				Patch:      0,
				PreRelease: "rc.10",
			},
		},
		{
			s: "0.0.0-20190718012654-fb15b899a751",
			want: semVer{
				Timestamp:          "20190718012654",
				RevisionIdentifier: "fb15b899a751",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			got, err := parseSemVer(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseSemVer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseSemVer() got = %v, want %v", got, tt.want)
			}
		})
	}
}
